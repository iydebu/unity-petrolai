using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Petroling : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public float thrust;
    public float distance;
    public float Walldistance;
    public bool isground;
    public bool isjumped;
    private bool movingRight = true;
    public Transform groundDetection;
    public Transform wallDetection;
    private Rigidbody2D rb;
    private Animator anim;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        isground = true;
        isjumped = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            anim.SetBool("ground", true);
            isground = true;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            anim.SetBool("ground", true);
            isground = true;
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            anim.SetBool("ground", false);
            isground = false;
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        transform.Translate(Vector2.left * speed * Time.fixedDeltaTime);
        RaycastHit2D wallinfoR, wallinfoL;
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        if (movingRight == false)
        {
            wallinfoR = Physics2D.Raycast(groundDetection.position, Vector2.right, Walldistance);
            Debug.DrawRay(wallDetection.position, Vector2.right * Walldistance, Color.red);
            if (wallinfoR.collider == true && !isjumped)
            {
                isjumped = true;
                rb.AddForce(transform.up * wallinfoR.transform.localScale.y * thrust );
                StartCoroutine(jumpCoolDown());
            }
        }
        else
        {
            wallinfoL = Physics2D.Raycast(groundDetection.position, Vector2.left, Walldistance);
            Debug.DrawRay(wallDetection.position, Vector2.left * Walldistance, Color.red);
            if (wallinfoL.collider == true && isjumped)
            {
                isjumped = true;
                rb.AddForce(transform.up * wallinfoL.transform.localScale.y * thrust);
                StartCoroutine(jumpCoolDown());
            }
        }
        Debug.DrawRay(groundDetection.position, Vector2.down * distance, Color.green);
        
        if (groundInfo.collider == false && isground)
        {
            if (movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }
    
    IEnumerator jumpCoolDown()
    {
        yield return new WaitForSeconds(1);
        isjumped = false;
    }
}
